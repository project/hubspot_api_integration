<?php

namespace Drupal\hubspot_api_integration;

/**
 * Interface VcitaAPIInterface.
 */
interface HubspotHelperInterface {

  const Contact_To_Company = 1;
  const Company_To_Contact = 2;
  const Deal_to_contact = 3;
  const Contact_to_deal = 4;
  const Deal_to_company = 5;
  const Company_to_deal = 6;
  const Company_to_engagement = 7;
  const Engagement_to_company	 = 8;
  const Contact_to_engagement	 = 9;
  const Engagement_to_contact	 = 10;
  const Deal_to_engagement	 = 11;
  const Engagement_to_deal	 = 12;
  const Parent_company_to_child_company	 = 13;
  const Child_company_to_parent_company	 = 14;
  const Contact_to_ticket	 = 15;
  const Ticket_to_contact	 = 16;
  const Ticket_to_engagement	 = 17;
  const Engagement_to_ticket	 = 18;
  const Deal_to_line_item	 = 19;
  const Line_item_to_deal	 = 20;
  const Company_to_ticket	 = 25;
  const Ticket_to_company	 = 26;
  const Deal_to_ticket	 = 27;
  const Ticket_to_deal	 = 28;

  /**
   * @param string $email
   *  Email address.
   * @return mixed
   *   Contact data or False.
   */
  public function getHubspotContact($email);

  /**
   * @param array $properties
   *  Array with contact properties.
   * @return mixed
   *   Contact data or False.
   */
  public function createHubspotContact($properties);

  /**
   * @param int $contact_id
   *  Hubspot contact id.
   * @param string $note_body
   *  Note body.
   * @param string $note_header
   *  First line of the note.
   * @return mixed
   *   Note data or False.
   */
  public function createHubspotContactNote($contact_id, $note_body, $note_header = '');

  /**
   * @param array $properties
   *  Array with tiket properties.
   * @return mixed
   *   Contact data or False.
   */
  public function createHubspotTicket($properties);

  /**
   * @param array $properties
   *  Array with tiket properties.
   * @return mixed
   *   Contact data or False.
   */
  public function createHubspotAssociations($from, $to, $type);

}
