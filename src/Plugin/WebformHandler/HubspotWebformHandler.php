<?php

namespace Drupal\hubspot_api_integration\Plugin\WebformHandler;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\hubspot_api_integration\HubspotHelperInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\webform\WebformSubmissionConditionsValidatorInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Create a Hubspot Contact from Webform submission.
 *
 * @WebformHandler(
 *   id = "hubspot_handler",
 *   label = @Translation("Hubspot"),
 *   category = @Translation("Web services"),
 *   description = @Translation("Creates a Contact in Hubspot when a form is submitted."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class HubspotWebformHandler extends WebformHandlerBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Hubspot API helper.
   *
   * @var \Drupal\hubspot_api_integration\HubspotHelperInterface
   */
  protected $hubspotHelper;

  /**
   * Constructs a HubspotWebformHandler object.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\webform\WebformSubmissionConditionsValidatorInterface $conditions_validator
   *   The webform submission conditions (#states) validator.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\hubspot_api_integration\HubspotHelperInterface $hubspot_helper
   *   Hubspot API helper.
   *
   * @see \Drupal\webform\Entity\Webform::getHandlers
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    LoggerChannelFactoryInterface $logger_factory,
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    WebformSubmissionConditionsValidatorInterface $conditions_validator,
    ModuleHandlerInterface $module_handler,
    HubspotHelperInterface $hubspot_helper
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger_factory, $config_factory, $entity_type_manager, $conditions_validator);
    $this->moduleHandler = $module_handler;
    $this->hubspotHelper = $hubspot_helper;

  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('webform_submission.conditions_validator'),
      $container->get('module_handler'),
      $container->get('hubspot_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [
      '#markup' => $this->t('Hubspot integration'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // Getting all webform fields.
    $fields = $this->webform->getElementsDecodedAndFlattened();

    // Getting all supported fields.
    $supported_fields = $this->getSupportedFields();

    foreach ($fields as $field_key => $field) {
      if (in_array($field['#type'], $supported_fields)) {
        $form[$field_key] = [
          '#title' => $field['#title'],
          '#type' => 'textfield',
          '#default_value' => !empty($this->configuration['hubspot_mapping'][$field_key]) ? $this->configuration['hubspot_mapping'][$field_key] : '',
          '#attributes' => ['placeholder' => $this->t('Enter Hubspot field mapping. E.g. "email", "phone" etc.')],
        ];
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    foreach ($form_state->getValues() as $key => $value) {
      $this->configuration['hubspot_mapping'][$key] = $value;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    $is_completed = ($webform_submission->getState() == WebformSubmissionInterface::STATE_COMPLETED);
    if ($is_completed) {
      try {
        $properties = $this->getHubspotFields($webform_submission);
        if (isset($properties['note'])) {
          $note = $properties['note']['value'];
          unset($properties['note']);
        }
        if (empty($properties['email'])) {
          $error_message = 'Email field should be added to webform and configured for Hubspot.';
        }
        else {
          // Checking if contact exists.
          $contact = $this->hubspotHelper->getHubspotContact($properties['email']['value']);
          // Creating new Contact in Hubspot.
          if (!$contact) {
            $contact = $this->hubspotHelper->createHubspotContact($properties);
          }
          if (!empty($note)) {
            // Creating new note for the contact.
            $this->hubspotHelper->createHubspotContactNote($contact['vid'], $note, 'Sent from webform ' . $webform_submission->getWebform()->label());
          }
        }
      }      catch (RequestException $e) {
        watchdog_exception('hubspot', $e);
        $error_message = '@form webform failed to create the Hubspot Contact.';
      }

      // Log error message if it exists.
      if (!empty($error_message)) {
        $context = [
          '@form' => $this->getWebform()->label(),
          'link' => $this->getWebform()->toLink($this->t('Edit handlers'), 'handlers')->toString(),
        ];
        $this->getLogger()->error($error_message, $context);
      }
    }
  }

  /**
   * Returns list of Hubspot-friendly webform fields.
   *
   * @return array
   *   List with webform fields which have Hubspot support.
   */
  public function getSupportedFields() {
    return [
      'email',
      'textfield',
      'textarea',
      'select',
      'tel',
    ];
  }

  /**
   * Returns list of Hubspot fields from webform submission using fields mapping.
   *
   * @param WebformSubmissionInterface $webform_submission
   *   Webform submission.
   *
   * @return array
   *   Mapped Hubspot fields ready to be sent.
   */
  public function getHubspotFields(WebformSubmissionInterface $webform_submission) {

    $supported_fields = $this->getSupportedFields();

    $webform = $webform_submission->getWebform();

    // Fields to be sent in a Hubspot request.
    $hubspot_fields = [];
    foreach ($webform_submission->getData() as $field => $value) {
      $webform_field = $webform->getElement($field);
      $webform_field_type = $webform_field['#type'];

      // Skip not supported field types.
      if (!in_array($webform_field_type, $supported_fields)) {
        continue;
      }

      // Getting Hubspot-friendly field value.
      $hubspot_field_value = $webform_submission->getElementData($field);

      // Contains actual Hubspot field machine name or empty string.
      $hubspot_field_name = $this->configuration['hubspot_mapping'][$field];

      // Apply mapping defined in handler configuration.
      if (!empty($hubspot_field_name)) {
        $hubspot_fields[$hubspot_field_name] = [
          'property' => $hubspot_field_name,
          'value' => $hubspot_field_value,
        ];
      }
    }

    $hubspot_fields['hs_lead_status'] = [
      'property' => 'hs_lead_status',
      'value' => "NEW",
    ];
    $hubspot_fields['source'] = [
      'property' => 'source',
      'value' => "Form",
    ];
    $hubspot_fields['source_form_name'] = [
      'property' => 'source_form_name',
      'value' => $this->webform->label(),
    ];
    $hubspot_fields['source_site_name'] = [
      'property' => 'source_site_name',
      'value' => \Drupal::request()->getSchemeAndHttpHost(),
    ];
    $hubspot_fields[] = [
      'property' => 'website_source',
      'value' => \Drupal::request()->getRequestUri(),
    ];

    return $hubspot_fields;
  }
}
