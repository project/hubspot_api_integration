<?php

namespace Drupal\hubspot_api_integration;

use Drupal\hubspot_api\Manager;

/**
 * HubspotHelper service.
 */
class HubspotHelper implements HubspotHelperInterface {

  /**
   * Hubspot manager.
   *
   * @var \Drupal\hubspot_api\Manager
   */
  protected $hubspotManager;

  /**
   * Constructs a new HubspotHelper object.
   *
   * @param \Drupal\hubspot_api\Manager $hubspot_manager
   *   Hubspot manager.
   */
  public function __construct(Manager $hubspot_manager) {
    $this->hubspotManager = $hubspot_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getHubspotContact($email) {
    try {
      $contact = $this->hubspotManager->getHandler()->contacts()->getByEmail($email);
      return $contact->toArray();
    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createHubspotContact($properties) {
    try {
      $contact = $this->hubspotManager->getHandler()->contacts()->create($properties);
      return $contact->toArray();
    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createHubspotTicket($properties) {
    try {
      $ticket = $this->hubspotManager->getHandler()->tickets()->create($properties);
      return $ticket->toArray();
    }
    catch (\Exception $e) {
      watchdog_exception('hubspot', $e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createHubspotAssociations($from_id, $to_id, $type) {
    try {
      $properties = [
        'fromObjectId' => $from_id,
        'toObjectId' => $to_id,
        'category' => 'HUBSPOT_DEFINED',
        'definitionId' => $type,
      ];
      $association = $this->hubspotManager->getHandler()->crmAssociations()->create($properties);
      return $association->toArray();
    }
    catch (\Exception $e) {
      watchdog_exception('hubspot', $e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createHubspotContactNote($contact_id, $note_body, $note_header = '') {
    try {
      $engagement = [
        'active' => 1,
        'type' => 'NOTE',
      ];
      $associations = [
        'contactIds' => [$contact_id],
      ];
      $body = $note_header ? ['<p>' . $note_header . '</p>', $note_body] : [$note_body];
      $metadata = [
        'body' => implode("\n", $body),
      ];
      $note = $this->hubspotManager->getHandler()->engagements()->create($engagement, $associations, $metadata);
      return $note->toArray();
    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

}
